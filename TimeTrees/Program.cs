﻿using System;
using System.Globalization;
using Newtonsoft.Json;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Linq;


namespace TimeTrees

{
    struct Person
    {
        public int Id;
        public string Name;
        public DateTime Birth;
        public DateTime Death;
    }

    struct TimelineEvent
    {
        public DateTime Date;
        public string Description;
    }

    class Program
    {
        private const int TimelineDateIndex = 0;
        private const int TimelineDescriptionIndex = 1;

        private const int IdIndex = 0;
        private const int NameIndex = 1;
        private const int BirthIndex = 2;
        private const int DeathIndex = 3;

        private const string PathTimeline = "..\\..\\..\\..\\timeline.csv";
        private const string PathPeople = "..\\..\\..\\..\\people.csv";
        private const string PathTimelineJson = "..\\..\\..\\..\\timeline.json";
        private const string PathPeopleJson = "..\\..\\..\\..\\people.json";

        static void Main(string[] args)
        {
            Console.WriteLine("1-people.json или 2-people.csv?");
            string peopleDet;
            peopleDet = Console.ReadLine();
            bool isJsonePeople = (peopleDet == "1");

            Console.WriteLine("1-timeline.json или 2-timeline.csv?");
            string timelineDet;
            timelineDet = Console.ReadLine();
            bool isJsoneTimeline = (timelineDet == "1");

            TimelineEvent[] timeline = isJsonePeople ? TimelineEventReadJson(PathTimelineJson) : TimelineEventReader(PathTimeline);
            Person[] people = isJsoneTimeline ? PeopleReadJson(PathPeopleJson) : PeopleReader(PathPeople);

            JsonWrite(PathPeopleJson, people);
            JsonWrite(PathTimelineJson, timeline);

            (int years, int months, int days) = DeltaMinAndMaxDate(timeline);

            Console.WriteLine($"Между максимальной и минимальной датами прошло: {years} лет, {months} месяцев и {days} дней");

            var peopleList = PeopleBurnInLeapYear(people);
        }

        static void JsonWrite(string path, object data)
        {
            if (!File.Exists(path))
            {
                string lines = JsonConvert.SerializeObject(data);
                File.WriteAllText(path, lines);
            }
        }

        static DateTime ParseDate(string value)
        {
            DateTime date;
            if (!DateTime.TryParseExact(value, "yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
            {
                if (!DateTime.TryParseExact(value, "yyyy-mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    if (!DateTime.TryParseExact(value, "yyyy-mm-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                    {
                        date = default;
                    }
                }
            }

            return date;
        }

        static string[][] DataReader(string path)
        {
            string[] data = File.ReadAllLines(path);
            string[][] splitData = new string[data.Length][];
            for (var i = 0; i < data.Length; i++)
            {
                var line = data[i];
                string[] parts = line.Split(";");
                splitData[i] = parts;
            }

            return splitData;
        }

        static (DateTime, DateTime) GetMinAndMaxDate(TimelineEvent[] timeline)
        {
            DateTime minDate = DateTime.MaxValue;
            DateTime maxDate = DateTime.MinValue;
            foreach (var timeEvents in timeline)
            {
                DateTime date = timeEvents.Date;
                if (date < minDate) minDate = date;
                if (date > maxDate) maxDate = date;
            }

            return (minDate, maxDate);
        }

        static (int, int, int) DeltaMinAndMaxDate(TimelineEvent[] timeline)
        {
            (DateTime minDate, DateTime maxDate) = GetMinAndMaxDate(timeline);
            return (maxDate.Year - minDate.Year,
                maxDate.Month - minDate.Month,
                maxDate.Day - minDate.Day);
        }

        static Person[] PeopleReadJson(string path)
        {
            string jsonPerson = File.ReadAllText(path);
            return JsonConvert.DeserializeObject<Person[]>(jsonPerson);
        }

        static Person[] PeopleReader(string path)
        {
            string[][] data = DataReader(path);
            
            
            Person[] people = new Person[data.Length];
            for (var i = 0; i < data.Length; i++)
            {
                var parts = data[i];
                Person person = new Person();
                person.Id = int.Parse(parts[IdIndex]);
                person.Name = parts[NameIndex];
                person.Birth = ParseDate(parts[BirthIndex]);
                if (parts.Length == 4)
                {
                    person.Death = ParseDate(parts[DeathIndex]);
                }
                else
                {
                    person.Death = DateTime.Now;
                }

                people[i] = person;
            }

            return people;
            
        }

        static Person[] PeopleBurnInLeapYear(Person[] people)
        {
            var peopleList = new Person[people.Length];
            for (var i = 0; i < people.Length; i++)
            {
                peopleList[i] = people[i];
            }

            foreach (var person in peopleList)
            {
                var birth = person.Birth;
                var death = person.Death;

                if (DateTime.IsLeapYear(birth.Year) && (death.Year - birth.Year <= 20))
                    Console.WriteLine(person.Name);
            }

            return peopleList;
        }
        
        static TimelineEvent[] TimelineEventReadJson(string path) 
        {
            string jsonTimeline = File.ReadAllText(path);
            return JsonConvert.DeserializeObject<TimelineEvent[]>(jsonTimeline);
        }
        

        static TimelineEvent[] TimelineEventReader(string path)
        {
            string[][] data = DataReader(path);
            
            
            TimelineEvent[] events = new TimelineEvent[data.Length];
            for (var i = 0; i < data.Length; i++)
            {
                var parts = data[i];
                TimelineEvent timeline = new TimelineEvent();
                timeline.Date = ParseDate(parts[TimelineDateIndex]);
                timeline.Description = parts[TimelineDescriptionIndex];

                events[i] = timeline;
            }

            return events;
            
        }
    }
}
